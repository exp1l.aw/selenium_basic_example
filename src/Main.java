import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.YandexMainPage;

public class Main {

    public static void main(String[] args) {
//        Раскомментировать и указать путь к chromedriver
//        System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\chromedriver.exe");

        WebDriver driver = new ChromeDriver();
        driver.get("https://yandex.ru");

        YandexMainPage yandexMainPage = new YandexMainPage(driver);
        yandexMainPage.clickLoginBtn();

    }
}
