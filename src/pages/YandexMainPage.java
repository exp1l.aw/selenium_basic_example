package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class YandexMainPage extends AbstractPage {

    @FindBy(xpath = "//*[@class = 'button desk-notif-card__login-enter-expanded button_theme_bordergray button_size_m i-bem button_js_inited']")
    private WebElement loginBtn;

    public YandexMainPage(WebDriver driver) {
        super(driver);
    }

    public void clickLoginBtn(){
        loginBtn.click();
    }

}
